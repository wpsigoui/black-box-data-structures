#include "stdio.h"
#include <iostream>
#include "blackbox.h"
#include <thread>
#define NUM_THREADS 2

using namespace std;

typedef unsigned long key_type;
typedef void *val_type;

volatile bool start = false;

void tinsert(const int tid, BlackBox<key_type, val_type> *box)
{
        while (!start)
        {
        }
        for (long long i = tid * 1000 + 1; i <= (tid + 1) * 1000; i++)
        {
                auto res = box->executeConcurrent(tid, Operation::insertIfAbsent, (void *)i, (void *)i);
                assert(!res);
        }
        
}


void terase(const int tid, BlackBox<key_type, val_type> *box)
{
        while (!start)
        {
        }
        for (long long i = ((tid + 1) % NUM_THREADS) * 1000 + 1; i <= (((tid + 1) % NUM_THREADS) + 1) * 1000; i++)
        {
                assert(box->executeConcurrent(tid, Operation::erase, (void *)i) == (void*)i);
        }
}


int main()
{
        auto box = new BlackBox<key_type, val_type>(NUM_THREADS, 1, 100000);

        std::thread *threads[NUM_THREADS];
        for (int i = 0; i < NUM_THREADS; ++i)
        {
                threads[i] = new std::thread(tinsert, i, box);
        }
        __sync_synchronize();
        start = true;

        for (int i = 0; i < NUM_THREADS; ++i)
        {
                threads[i]->join();
                delete threads[i];
        }
        box->validate();

        start = false;
              __sync_synchronize();

        for (int i = 0; i < NUM_THREADS; ++i)
        {
                threads[i] = new std::thread(terase, i, box);
        }
        __sync_synchronize();
        start = true;

        for (int i = 0; i < NUM_THREADS; ++i)
        {
                threads[i]->join();
                delete threads[i];
        }

        box->validate();


        // cout << "Single Thread Insert " << (val_type)box->executeConcurrent(0, insertIfAbsent, (void *)1, (void *)5) << endl;
        // cout << "Single Thread Contains " << (bool)box->executeConcurrent(0, contains, (void *)1) << endl;

        // cout << "Single Thread Insert " << (val_type)box->executeConcurrent(1, insertIfAbsent, (void *)1, (void *)10) << endl;
        // cout << "Single Thread Contains " << (bool)box->executeConcurrent(1, contains, (void *)1) << endl;
}
