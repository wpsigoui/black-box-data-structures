FLAGS =
LDFLAGS =
GPP = g++

FLAGS+=-g
FLAGS+=-O3


LDFLAGS += -lpthread
LDFLAGS += -ldl
LDFLAGS += -I ds/avl/ -I locks/

bin_dir=bin
dir_guard:
	@mkdir -p $(bin_dir)

test: dir_guard
	$(GPP) main.cpp -o $(bin_dir)/test.out $(FLAGS) $(LDFLAGS)


clean:
	rm $(bin_dir)/*.out